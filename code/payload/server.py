from http.server import BaseHTTPRequestHandler, HTTPServer
import socketserver

class S(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self._set_headers()
        x = input("--> ")
        try:
            self.wfile.write(str.encode(x))
        except:
            print("that dont worked, probably "
                  "timeout or the host is unreachable")
            print("please try again!")
        
    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
        self._set_headers()
        print("::: ")
        self.data_string = self.rfile.read(
                int(self.headers['Content-Length']))

        self.send_response(200)
        self.end_headers()
        awns = str.replace(
                str.replace(
                    str.replace(
                        str.replace(
                            str(self.data_string),
                            "\\r\\n","\n"),
                        "\\n\\r","\n"),
                    "'",""),
                '"','')[1:]
        if(awns=="BYE/SEU"):
            print(awns)
            exit(0)
        print(awns)


def run(server_class=HTTPServer, handler_class=S, port=80):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print('Listing on Port '+ str(port))
    httpd.serve_forever()

if __name__ == "__main__":
    print(" _   _______  _____   _____                 _ _____ ")
    print("| | | | ___ \\/  ___| /  __ \\               | /  __ \\")
    print("| | | | |_/ /\\ `--.  | /  \\/ __ _ _ __   __| | /  \\/")
    print("| | | | ___ \ `--. \ | |    / _` | '_ \ / _` | |    ")
    print("\\ \\_/ / |_/ //\\__/ / | \__/\\ (_| | | | | (_| | \\__/\\")
    print(" \\___/\\____/ \\____/   \\____/\\__,_|_| |_|\\__,_|\\____/")
    
    print("\n\n To leave just send the command /close \n")
    run()
