var OneSignal = window.OneSignal || [];
OneSignal.push(["init", {
appId: "f0964f83-6f53-4152-b9bd-aee6ee68a440",
autoRegister: false,
notifyButton: {
enable: true,
position: 'bottom-left',
showCredit: false,
prenotify: true,
displayPredicate: function() {
  return OneSignal.isPushNotificationsEnabled()
    .then(function(isPushEnabled) { return !isPushEnabled;});
},
text: {
  'dialog.blocked.message': "Follow these instructions to allow instant discount coupons and important notifications:"
}
},
welcomeNotification: {
"title": "Thank you for subscribing!",
"message": "Get 10% off all products today, please use coupon code PU10SH, enjoy :)",
"url": "https://www.moddiy.com/pages/Thank-you-for-subscribing%21.html" 
}
}]);

function sleep(ms) {
return new Promise(res => setTimeout(res, ms));
}
let myPush = async function() {
await sleep(10000);
OneSignal.push(function() {OneSignal.registerForPushNotifications();});
}
myPush();