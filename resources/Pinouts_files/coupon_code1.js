if(typeof(asset_host) != 'string') {var asset_host = '//cdn.justuno.com/'};

asset_host = (asset_host.indexOf('cloudfront')>-1 ? '//cdn.justuno.com/' : (asset_host.indexOf('cdn.justuno.com')>-1 ? '//cdn.justuno.com/' : asset_host));
var ju_domain = (asset_host.indexOf('cdn.justuno.com')>1 || asset_host.indexOf('www.justuno.com')>1 || asset_host.indexOf('cloudfront')>1 || asset_host.indexOf('amazon')>1 ? 'https://www.justuno.com' : 'https://'+asset_host.split('/')[2]);
//asset_host = '//cdn.justuno.com/';

function ju_loadversionscript(v){
	(function() {var s=document.createElement('script');s.type='text/javascript';s.async=true;s.src=asset_host+'mwgt_'+v+'.js';var x=document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);})();
}

  function jju_setCookie( name, value, days, path ){
    // set time, it's in milliseconds
    var today = new Date();
    today.setTime(today.getTime()+(days*24*60*60*1000));
    
    document.cookie = name + "=" +escape( value ) +
    ";expires=" + (days==-1 ? 'Thu, 01 Jan 1970 00:00:01 GMT' : today.toGMTString()) +
    ( ( path ) ? ";path=" + path : "" );
  }

function jju_getCookie(c_name){
  var i,x,y,ARRcookies=document.cookie.split(";");
  for (i=0;i<ARRcookies.length;i++)
  {
    x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
    y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
    x=x.replace(/^\s+|\s+$/g,"");
    if (x==c_name) {
    return unescape(y);
    }
  }
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}


var $jujsonp = (function(){
  var that = {};

  that.send = function(src, options) {
    var callback_name = options.callbackName || 'callback',
      on_success = options.onSuccess || function(){},
      on_timeout = options.onTimeout || function(){},
      timeout = options.timeout || 10; // sec

    var timeout_trigger = window.setTimeout(function(){
      window[callback_name] = function(){};
      on_timeout();
    }, timeout * 1000);

    window[callback_name] = function(data){
      window.clearTimeout(timeout_trigger);
      on_success(data);
    }

    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.async = true;
    script.src = src;

    document.getElementsByTagName('head')[0].appendChild(script);
  }

  return that;
})();

var ju_v = jju_getCookie("_ju_v")
if (isNumeric(ju_v) && parseFloat(ju_v)>1){
  jju_setCookie("_ju_v", ju_v,.0208,"/");
  ju_loadversionscript(ju_v);
} else {
  if (ju_num.match(/^[{]?[0-9a-fA-F]{8}[-]?([0-9a-fA-F]{4}[-]?){3}[0-9a-fA-F]{12}[}]?$/)) {

    $jujsonp.send(ju_domain+'/ajax/account_version_check.html?id='+ju_num, {
        callbackName: 'ju_vcheck',
        onSuccess: function(json){
            //console.log('success!', json);
            ju_v = json.v;
            jju_setCookie("_ju_v", ju_v,.0208,"/");
            ju_loadversionscript(ju_v);
        }
    });

  }
}